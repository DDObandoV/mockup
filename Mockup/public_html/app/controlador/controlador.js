/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function () {
    'use strict';
    //Aquí se realiza la declaración del controlador
    //Siempre llamar tanto a los archivos como al alias del controlador con el nombre de la funcionalidad
    //y así no seguir un estandar de codificación.
    angular.module('mockup').controller('controlador', controlador);
    //Se realiza la inyección de dependencias que necesitmos utilizar en nuestro controlador.
    controlador.$inject = [];
    function controlador() {
        var control = this;
        control.entregas = [
            {   id:0,
                fecha : "Monday 10 2:28 PM",
                origen:"Houston,TX,33619",
                destino:"Atlanta,GA,30123",
                precio:"$250.00",
                cantidad:1
            }, {
                id:1,
                fecha : "Monday 10  2:28 PM",
                origen:"Houston,TX,33619",
                destino:"Atlanta,GA,30123",
                precio:"$250.00",
                cantidad:1
            }, {
                id:2,
                fecha : "Monday 10 2:28 PM",
                origen:"Houston,TX,33619",
                destino:"Atlanta,GA,30123",
                precio:"$250.00",
                cantidad:1
            }]
    }

})();





